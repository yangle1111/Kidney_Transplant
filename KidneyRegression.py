# Part 1 - Data Preprocessing

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import os
os.environ["THEANO_FLAGS"] = "device=cuda0"

# Importing the dataset
dataset = pd.read_csv('Sample10K.csv')
X = dataset.iloc[:, 2:1070].values
y = dataset.iloc[:, 1070:1071].values      
                
#try threshold 5 years as boolean
#y = dataset.iloc[:,1073:1074].values                 

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Importing the Keras libraries and packages
import keras
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from keras.models import Sequential
from keras.layers import Dense

# Part 3 - Making predictions and evaluating the model

# Initialising the ANN
def build_classifier():
    classifier = Sequential()
    classifier.add(Dense(units = 48, kernel_initializer = 'normal', activation = 'relu', input_dim = 1068))
    classifier.add(Dense(units = 48, kernel_initializer = 'normal', activation = 'relu'))
    classifier.add(Dense(units = 48, kernel_initializer = 'normal', activation = 'relu'))
    classifier.add(Dense(units = 1, kernel_initializer = 'normal', activation = 'linear'))
    classifier.compile(optimizer = 'adam', loss = 'mean_squared_error', metrics = ['mse','accuracy'])
    return classifier

seed = 7
np.random.seed(seed)
classifier = KerasRegressor(build_fn=build_classifier, nb_epoch=100, batch_size=5)

#print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))
#classifier = KerasClassifier(build_fn = build_classifier, batch_size = 10, nb_epoch = 100)
kfold = KFold(n_splits=10, random_state=seed)
results = cross_val_score(classifier, X, y, cv=kfold)
accuracies = cross_val_score(estimator = classifier, X = X_train, y = y_train, cv = 3, n_jobs = -1)
#mean = accuracies.mean()
#variance = accuracies.std()

classifier.fit(X_train, y_train, batch_size = 10, epochs = 400)
# Predicting the Test set results
y_pred = classifier.predict(X_test)
#y_pred = (y_pred > 0.5)


# Making the Confusion Matrix
#from sklearn.metrics import confusion_matrix
#cm = confusion_matrix(y_test, y_pred)
#print (cm)
